# idea

The idea is relatively simple. We have interesting work on sentence level vectorization from UKPLab and we have 
new functionality for handling cosine similarity in elasticsearch. 

This is a minor work to get my hands dirty, akin to a poc. 

# sources

based on https://github.com/UKPLab/sentence-transformers. 

borrowed freely from https://towardsdatascience.com/elasticsearch-meets-bert-building-search-engine-with-elasticsearch-and-bert-9e74bf5b4cf2

# config

I work with python 3.7 on an ubuntu linux variant. Seems there is too little compatible with 3.8 yet (notably tf). 

I work with a virtual env; `conda create --name XXX_py37 python=3.7` and activate. 

You need torch installed - there were for me some c++ libs that were missing. E.g. `conda install pytorch torchvision cpuonly -c pytorch`

Then you need `pip install -r requirements.txt`

## docker

`$ docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.6.2`

# example 1

1. Create the index; `python example/create_index.py --index_file=example/index.json --index_name=jobsearch`
1. Submit document vectors (this is where we work with sentence-tranformers); `python example/create_documents.py --data=example/example.csv --index_name=jobsearch`
1. Index the documents; `python example/index_documents.py`
1. Make a query; `python example/query.py --query_string="Any lawyers in here?" --index_name=jobsearch` 

## details

An indexed document looks like; 

```
{
  "hits": [
    <..>
    {
      "_id": "iQ7zbXEBLCaqS6gdvXpg",
      "_index": "jobsearch",
      "_score": 1.0,
      "_source": {
        "text": "lorem ipsum",
        "text_vector": [
          -0.06638193875551224,
          -0.25907549262046814,
          2.0879218578338623,
  <..>
          0.029935652390122414
        ],
        "title": "Network Administrator"
      },
      "_type": "_doc"
    }
  ],
  "max_score": 1.0,
  "total": {
    "relation": "eq",
    "value": 5
  }
}

Process finished with exit code 0
