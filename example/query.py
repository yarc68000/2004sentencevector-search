import argparse
from sentence_transformers import SentenceTransformer

from elasticsearch import Elasticsearch
import json

# cf. https://www.elastic.co/blog/text-similarity-search-with-vectors-in-elasticsearch &  https://github.com/jtibshirani/text-embeddings/blob/blog/src/main.py

model = SentenceTransformer('bert-base-nli-mean-tokens')


def vectorize_query(query_string):
    return model.encode(query_string)[0].tolist()


def main(args):
    client = Elasticsearch()
    client.indices.refresh(index=args.index_name)

    query_vector = vectorize_query(args.query_string)

    query = {
        "script_score": {
            "query": {"match_all": {}},
            "script": {
                "source": "cosineSimilarity(params.query_vector, 'text_vector') + 1.0",
                "params": {"query_vector": query_vector}
            }
        }
    }

    #    res = client.search(index=args.index_name, body={"query": {"match_all": {}}})
    res = client.search(
        index=args.index_name,
        body={
            "size": 5,
            "query": query,
            "_source": {"includes": ["text", "title"]}
        }
    )
    print("done:\n{}".format(json.dumps(res["hits"], indent=2, sort_keys=True)))




if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Making similarity queries')
    parser.add_argument('--index_name', default='jobsearch', help='Elasticsearch index name.')
    parser.add_argument('--query_string', default='lorem ipsum', help='What are we searching for?')
    args = parser.parse_args()
    main(args)




